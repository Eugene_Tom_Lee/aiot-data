package net.srt.api;

/**
 * 服务名称集合
 *
 * @author 阿沐 babamu@126.com
 */
public interface ServerNames {
	/**
	 * srt-cloud-gateway 服务名
	 */
	String GATEWAY_SERVER_NAME = "srt-cloud-gateway";
    /**
     * srt-cloud-system 服务名
     */
    String SYSTEM_SERVER_NAME = "srt-cloud-system";
    /**
     * srt-cloud-message 服务名
     */
    String MESSAGE_SERVER_NAME = "srt-cloud-message";

	/**
	 * srt-cloud-quartz 服务名
	 */
	String QUARTZ_SERVER_NAME = "srt-cloud-quartz";

	/**
	 * srt-cloud-data-integrate 服务名
	 */
	String DATA_INTEGRATE_NAME = "srt-cloud-data-integrate";

	/**
	 * srt-cloud-data-development 服务名
	 */
	String DATA_DEVELOPMENT_NAME = "srt-cloud-data-development";

	/**
	 */
	String DEVELOP_FLINK_114 = "srt-cloud-develop-flink114";

	/**
	 */
	String DEVELOP_FLINK_116 = "srt-cloud-develop-flink116";

	/**
	 * srt-cloud-data-governance 服务名
	 */
	String DATA_GOVERNANCE_NAME = "srt-cloud-data-governance";

	/**
	 * srt-cloud-data-governance 服务名
	 */
	String DATA_SERVICE_NAME = "srt-cloud-data-service";
}
