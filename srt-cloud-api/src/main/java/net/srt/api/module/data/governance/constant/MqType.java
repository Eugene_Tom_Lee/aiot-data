package net.srt.api.module.data.governance.constant;

/**
 * @ClassName MqType
 * @Author zrx
 * @Date 2023/10/8 11:06
 */
public enum MqType {
	kafka,
	rabbitMq
}
