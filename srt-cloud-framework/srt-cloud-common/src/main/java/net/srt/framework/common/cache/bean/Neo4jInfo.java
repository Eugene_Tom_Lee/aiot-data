package net.srt.framework.common.cache.bean;

import lombok.Data;

/**
 * @ClassName Neo4jInfo
 * @Author zrx
 * @Date 2023/6/13 17:43
 */
@Data
public class Neo4jInfo {
	private String neo4jUrl;
}
